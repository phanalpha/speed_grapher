FROM node:14

EXPOSE 3000

WORKDIR /usr/src

COPY . .

RUN apt-get update && \
	apt-get install -y \
	libnss3 \
	libatk-bridge2.0-0 \
	libx11-xcb1 \
	libxcb-dri3-0 \
	libdrm2 \
	libgbm1 \
	libasound2 \
	libxss1 \
	libgtk-3-0 && \
	yarn

CMD ["node", "index.js", "--host", "0.0.0.0"]
