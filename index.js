const packageJson = require('./package.json')

const express = require('express')
const puppeteer = require('puppeteer')

;(async () => {
  const browser = await puppeteer.launch({
    args: [
      '--no-sandbox',
      '--disable-setuid-sandbox'
    ]
  })
  const userAgent = await browser.userAgent()

  const { argv } = require('yargs')
    .scriptName(packageJson.name)
    .env(packageJson.name)
    .command('*', '', yargs => {
      yargs
	.option('host', {
	  default: 'localhost',
	})
	.option('port', {
	  default: 3000,
	})
	.option('max-pages', {
	  type: 'number',
	  default: 10,
	})
	.option('user-agent', {
	  default: `${userAgent} ${packageJson.name}`,
	})
    })

  const { Sema } = require('async-sema')
  const s = new Sema(argv.maxPages)

  const app = express()
  const pages = []

  app.get('/:path(*)', async (req, res) => {
    const scheme = req.get('X-Forwarded-Proto')
    const host = req.get('X-Forwarded-Host')
    const port = req.get('X-Forwarded-Port')

    const url = `${scheme}://${host}${port ? ':' + port : ''}/${req.params.path}`

    console.log(url)

    await s.acquire()
    try {
      const page = pages.pop() || await browser.newPage().then(async p => {
	if (argv.userAgent) {
	  await p.setUserAgent(argv.userAgent)
	}

	return p
      })

      try {
	await page.goto(url)

	res.send(await page.content())
      } catch {
      }

      pages.push(page)
    } finally {
      s.release()
    }
  })

  return app.listen(argv.port, argv.host, () => console.log(`Listening at http://${argv.host}:${argv.port}`))
})()
